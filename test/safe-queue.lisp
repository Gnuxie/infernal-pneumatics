#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:infernal-pneumatics.test)

(defmacro await-promise ((promise &key (timeout 1))
                         &body body)
  "hacky but whatever."
  (alexandria:with-gensyms (elapsed-time-sym timeout-sym)
    `(let ((,elapsed-time-sym 0)
           (,timeout-sym ,timeout))
       (loop :until (or (> ,elapsed-time-sym ,timeout-sym)
                        (bb:promise-finished-p ,promise))
          :do (sleep 0.1))
       (cond ((> ,elapsed-time-sym ,timeout-sym)
              (parachute:true nil "timed out"))

             (t ,@body)))))

(p:define-test simple
  :parent infernal-pneumatics.test
  (let ((tube (make-instance 'ip:safe-queue-tube)))
    (ip:spawn-thread tube)
    (let ((answer (ip:defer-task tube #'+ 2 2)))
      (await-promise (answer)
        (bb:alet ((answer answer))
          (p:is #'= 4 answer))))
    (ip:defer-task tube #'ip:stop-thread)))
