(asdf:defsystem #:infernal-pneumatics.test
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :depends-on ("infernal-pneumatics" "parachute")
  :serial t
  :components ((:file "package")
               (:file "safe-queue")))
