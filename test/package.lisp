#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:infernal-pneumatics.test
  (:use #:cl)
  (:local-nicknames
   (#:ip #:infernal-pneumatics)
   (#:p #:parachute))
  (:export
   #:run
   #:ci-run
   #:infernal-pneumatics.test))

(in-package infernal-pneumatics.test)

(p:define-test #:infernal-pneumatics.test)

(defun run (&key (report 'p:plain))
  (p:test 'infernal-pneumatics.test :report report))

(defun ci-run ()
  (let ((test-result (run)))
    (when (not (null (p:results-with-status :failed test-result)))
      (uiop:quit -1))))
