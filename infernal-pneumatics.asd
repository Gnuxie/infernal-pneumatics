(asdf:defsystem #:infernal-pneumatics
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license "Artistic License 2.0"
  :description "Currently this is quite 'low level' there are a few future abstractions
to be made, for example, several bordeaux threads can share one message box
and these message boxes will always have a given set of functions passed to them
this set can be realised (in a way they are in an actors system). Also any state
a 'worker' will refer to will be captured only by dynamic bindings, so there
is work to be done here too and then you've basically just made actors havn't you.

we should also consider hainvg messages without promises e.g. when the worker 'actor'
will send a message to another with the result to ensure what would be a callback
was executed within the expected dynamic environment. Although this really is just
saving a couple of promise chains."
  :depends-on ("safe-queue" "bordeaux-threads" "blackbird")
  :serial t
  :components ((:module "code" :components
                        ((:file "package")
                         (:file "tube")
                         (:file "task")
                         (:file "message")
                         (:file "safe-queue")
))))
