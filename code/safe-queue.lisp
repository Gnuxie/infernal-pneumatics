#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:infernal-pneumatics)

(defclass safe-queue-event ()
  ((%timestamp :initarg :timestamp :reader timestamp
               :initform (get-internal-real-time))
   (%thread :initarg :thread :reader thread
            :initform (bt:current-thread))))

(defun safe-queue-event (tube message datum &rest arguments)
  (handle-event tube message
                (apply #'make-instance datum arguments)))

(defclass awaiting-message-event (safe-queue-event)
  ())

(defclass received-message-event (safe-queue-event)
  ())

(defclass thread-stopped-event (safe-queue-event)
  ())

(defgeneric time-delta (event)
  (:method ((event safe-queue-event))
    (- (get-internal-real-time)
       (timestamp event))))

;;; hmm i mean, this should be in another package tbh.
(defclass safe-queue-tube (tube)
  ((%mailbox :initarg :mailbox :reader mailbox :initform (safe-queue:make-mailbox))))

(defgeneric received-message (tube message)
  (:method ((tube safe-queue-tube) (message blackbird-function-message))
    (safe-queue-event tube message 'received-message-event)))

;;; we don't do anything if there's no handlers.
(defmethod handle-event ((tube safe-queue-tube) message event)
  (declare (ignore tube message event))
  nil)

(defgeneric calculate-special-bindings (tube &key special-bindings)
  (:documentation "This is called on spawn-thread to calculate the
dynamic bindings list to pass to bt.")
  (:method ((tube safe-queue-tube) &rest args &key (special-bindings bt:*default-special-bindings*) &allow-other-keys)
    (declare (ignore args))
    special-bindings))

(defmethod send ((tube safe-queue-tube) message)
  (safe-queue:mailbox-send-message (mailbox tube) message))

(defun %tube-thread (tube)
  (tagbody
   :start
     (safe-queue-event tube nil 'awaiting-message-event)
     (let ((message (safe-queue:mailbox-receive-message (mailbox tube))))
       (tagbody
        :message-start
          (handler-bind
              ((%stop-thread (lambda (c)
                               (declare (ignore c))
                               (invoke-restart 'stop-thread))))
            (restart-case
                (progn
                  (received-message tube message)
                  (process tube message))
              (try-message-again ()
                (go :message-start))

              (drop-message ()
                (go :start))

              (stop-thread ()
                (safe-queue-event tube message 'thread-stopped-event)
                (go :end))))))
     (go :start)
   :end
   nil))

(defmethod spawn-thread ((tube safe-queue-tube) &rest args &key &allow-other-keys)
  (let ((special-bindings (apply #'calculate-special-bindings tube args)))
    (bt:make-thread (lambda () (%tube-thread tube))
                    :initial-bindings special-bindings)))

(defmethod process ((tube safe-queue-tube) (message blackbird-function-message))
  (handler-bind ((error (lambda (c)
                          (unless blackbird:*debug-on-error*
                            (invoke-restart 'call-rejector c)))))
    (restart-case
        (let ((result (apply (message-function message) (message-arguments message))))
          (when (resolver message)
            (funcall (resolver message) result)))
      (call-rejector (condition)
        (when (rejector message)
          (funcall (rejector message) condition))))))

(defmethod send ((tube safe-queue-tube) (message blackbird-function-message))
  (bb:with-promise (resolve reject :resolve-fn resolver :reject-fn rejector)
    (setf (resolver message) resolver
          (rejector message) rejector)
    (call-next-method)))
