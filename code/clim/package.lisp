#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:infernal-pneumatics.clim
  (:use #:clim #:clim-lisp)
  (:local-nicknames
   (#:ip #:infernal-pneumatics))
  (:export
   #:thread-monitor
   #:display-thread-event
   #:update-thread-information
   #:display-thread-monitor))
