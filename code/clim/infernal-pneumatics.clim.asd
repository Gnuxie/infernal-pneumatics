(asdf:defsystem #:infernal-pneumatics.clim
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :depends-on ("mcclim")
  :components ((:file "package")
               (:file "status-pane")))
