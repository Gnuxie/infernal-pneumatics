#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:infernal-pneumatics.clim)

;;; collect information thread's from events
;;;
;;; isn't it better to display the actual event
;;; rather than the thread?
;;; YES.
;;; We instead have gfs that messages specialise
;;; To create their own subclass of the start event
;;; this way they can control the display function
;;; and show a task rather than a thread.
(defclass thread-monitor (clim-stream-pane)
  ((%thread-information :initarg :thread-information
                        :accessor thread-information
                        :initform '())))

;;; it seems to be much easier to work with lists for one reason and one reason only.
;;; remove cannot guarantee that the result is a vector.
(defgeneric update-thread-information (thread-monitor information)
  (:method ((monitor thread-monitor) (event ip:safe-queue-event))
    (let ((existing-entry
            (find (ip:thread event) (thread-information monitor) :key #'ip:thread)))
      (if existing-entry
          (setf (thread-information monitor)
                (substitute event existing-entry (thread-information monitor)))
          (setf (thread-information monitor)
                (push event (thread-information monitor))))))
  (:method ((monitor thread-monitor) (event ip:thread-stopped-event))
    (setf (slot-value monitor '%thread-information)
          (remove (ip:thread event) (thread-information monitor)
                  :key #'ip:thread)))
  (:method ((monitor thread-monitor) (event ip:awaiting-message-event))
    (setf (slot-value monitor '%thread-information)
          (remove (ip:thread event) (thread-information monitor)
                  :key #'ip:thread))))

(defgeneric display-thread-event (stream event)
  (:method (stream (event ip:awaiting-message-event))
    (surrounding-output-with-border (stream)
      (let ((time-delta (ip:time-delta event)))
        (format stream "IDLE for ~,2F second~:P"
                (/ time-delta internal-time-units-per-second)))))
  (:method (stream (event ip:received-message-event))
    (surrounding-output-with-border (stream)
      (format stream "WORKING for ~,2F second~:P"
              (/ (ip:time-delta event) internal-time-units-per-second)))))

;;; so i'm thinking that the stack should actually be displayed in reverse.
(defun display-thread-monitor (frame monitor)
  (declare (ignore frame))
  (when (< 0 (length (thread-information monitor)))
    (formatting-table (monitor)
      (formatting-row (monitor)
        (loop :for event :in (thread-information monitor)
              :do (formatting-cell (monitor :align-y :top)
                    (display-thread-event monitor event)))))))
