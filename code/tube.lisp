#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:infernal-pneumatics)

;;; Establishing the tube protocol.
(defclass tube ()
  ()
  (:documentation "This is really a focus on a mailbox that handles a set of
messages e.g. you may have a tube class for handling thumbnail loading."))

(defgeneric send (tube message)
  (:documentation "Send a message to a tube ready for processing."))

(defgeneric handle-event (tube message event)
  (:documentation "A tube may specify events related to the processing of a message
e.g. 'The tube is going to process the message, here's some information about the
circumstances'"))

(defgeneric process (tube message)
  (:documentation ""))

(defgeneric spawn-thread (tube &key)
  (:documentation "Spawn a thread to start working with the tube."))

(defgeneric spawned-threads (tube)
  (:documentation "Return the threads spawned from this tube."))

;;; It's a mistake to have a message protocol class because a message could
;;; be anything e.g. an integer or something in some cases.
