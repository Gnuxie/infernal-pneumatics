#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:infernal-pneumatics)

(defclass function-message ()
  ((%function :initarg :function :reader message-function
              :type function)
   (%arguments :initarg :arguments :reader message-arguments
               :type list :initform '())))

(defclass blackbird-function-message (function-message)
  ((%resolver :initarg :resolver :accessor resolver
              :type (or function null))
   (%rejector :initarg :rejector :accessor rejector
              :type (or function null))))

(defun defer-task (tube function &rest arguments)
  (send tube
        (make-instance 'blackbird-function-message
                       :function function
                       :arguments arguments)))

(defmacro with-defered-task ((tube) &body body)
  `(send
    ,tube
    (make-instance 'blackbird-function-message
                   :function (lambda () ,@body))))
