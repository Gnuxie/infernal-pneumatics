#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(defpackage #:infernal-pneumatics
  (:use #:cl)
  (:export
   ;; protocol
   #:tube
   #:send
   #:handle-event
   #:process
   #:spawn-thread

   ;; safe-queue
   #:stop-thread
   #:function-message
   #:blackbird-function-message

   #:safe-queue-event
   #:awaiting-message-event
   #:received-message-event
   #:received-message
   #:thread-stopped-event
   #:safe-queue-tube
   #:mailbox
   #:calculate-special-bindings
   #:timestamp
   #:thread
   #:time-delta

   ;; utils - possible bad tbh
   #:with-defered-task
   #:defer-task
   ))

(in-package #:infernal-pneumatics)
