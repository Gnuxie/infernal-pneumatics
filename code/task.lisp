#|
    Copyright (C) 2020 Gnuxie <Gnuxie@protonmail.com>
|#

(in-package #:infernal-pneumatics)

(define-condition %stop-thread (error)
   ())

(defun stop-thread ()
  (error '%stop-thread))
